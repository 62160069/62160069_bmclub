
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author BmWz
 */
public class OrderController {

    private static ArrayList<Order> orderList = new ArrayList<>();

    //create
    public static boolean addOrder(Order order) {
        orderList.add(order);
        save();
        return true;
    }

    //delete
    public static boolean delOrder(Order order) {
        orderList.remove(order);
        save();
        return true;
    }

    public static boolean delOrder(int index) {
        orderList.remove(index);
        save();
        return true;
    }

    //read
    public static ArrayList<Order> getOrder() {
        return orderList;
    }

    public static Order getOrder(int index) {
        return orderList.get(index);
    }

    //Update
    public static boolean updaeOrder(int index, Order order) {
        orderList.set(index, order);
        save();
        return true;
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        file = new File("Bm.dat");
        try {
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(orderList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;

        file = new File("Bm.dat");
        try {
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            orderList = (ArrayList<Order>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static boolean clear() {
        orderList.removeAll(orderList);
        save();
        return true;
    }

    public static int sizeArray() {
        return orderList.size();
    }
}
